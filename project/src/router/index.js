import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
// import Technologies from '@/components/Technologies'

Vue.use(Router)

// export default new Router({
  const routes = [
 
    {
      path: '/',
      component: Main
    },
    //   {
    //   path: '/',
    //   component: Education
    // }
  ]
// })
export default new Router({
  routes // short for `routes: routes`
})

// const app = new Vue({
  // router
// }).$mount('#app')
